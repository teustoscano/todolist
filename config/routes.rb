Memo::Application.routes.draw do
  
  devise_for :user
  root to: 'pages#home'
  resources :tasks, except: [:index]

end
