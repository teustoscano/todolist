class CreateTarefas < ActiveRecord::Migration
  def change
    create_table :tarefas do |t|
      t.string :titulo
      t.text :nota
      t.date :dia

      t.timestamps
    end
  end
end
